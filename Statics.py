from PIL import Image, ImageDraw
import numpy as np
from scipy import ndimage
import os


#This dictionary saves aspect ratio of each object instant, we use it to calculate
#average aspect ratios and aspect variance
obj_ratio = {}
obj_area = {}
for i in range(1,151):
    obj_ratio[i] = []
    obj_area[i] = []

annotationPath = '/home/lixueting/Documents/DATASET/MIT_Scene_Parsing_Chanllenge/ADEChallengeData2016/annotations/training/'
#annotationPath = '/home/lixueting/Dropbox/Ojbect_Statistic_Bolei/images'
list_dirs = os.walk(annotationPath)
print('Go through each image to collect object information...')
counter = 1
for root, dirs, files in list_dirs:
    for f in files:
        print('Image:%d'%(counter))
        counter = counter + 1
        image = Image.open(os.path.join(annotationPath,f))

        #Count Objects
        img = np.asarray(image)
        unique_obj = np.unique(img)
        for obj_num in unique_obj:
            if(obj_num != 0):
                img_map = (img == obj_num)
                img_array = img_map.astype(int)
                labeled_image, num_features = ndimage.label(img_array)
                # Label objects
                #labeled_image, num_features = ndimage.label(img_array)
                # Find the location of all objects
                objs = ndimage.find_objects(labeled_image)
                # Get the height and width
                for obj in objs:
                    #measurements.append((int(ob[0].stop - ob[0].start), int(ob[1].stop - ob[1].start)))
                    #cor = (int(obj[1].start), int(obj[0].start), int(obj[1].stop), int(obj[0].stop))
                    height = float(obj[0].stop - obj[0].start)
                    width = float(obj[1].stop - obj[1].start)
                    aspect_ratio = round(width/height,2)
                    area = round(width*height,2)
                    obj_ratio[obj_num].append(aspect_ratio)
                    obj_area[obj_num].append(area)

#calculate average object aspect ratio and area, variance
fw = open('object.txt','w')
print('Calculate variance and mean area&ratio of each object category...')
for i in range(1,151):
    print('Image:%d'%(i))
    ratio_list = obj_ratio[i]
    mean_ratio = np.mean(ratio_list)
    var_ratio = np.var(ratio_list)

    area_list = obj_area[i]
    mean_area = np.mean(area_list)
    var_area = np.var(area_list)

    fw.write('%d   %.2f     %.2f    %.2f    %.2f\n'%(i,mean_area,var_area,mean_ratio,var_ratio))
